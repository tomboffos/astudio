<?php


namespace App\Services;


use App\Http\Resources\Interactions\OrderResource;
use App\Models\Order;
use App\Models\User;

class OrderService
{

    public function registerMasters(Order $order,$masters)
    {
        foreach ($masters as $master){
            $order->masters()->save(User::find($master));
        }

        return new OrderResource($order);
    }
}
