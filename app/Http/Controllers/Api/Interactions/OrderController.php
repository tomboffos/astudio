<?php

namespace App\Http\Controllers\Api\Interactions;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Interactions\OrderRequest;
use App\Http\Resources\Interactions\OrderResource;
use App\Models\Order;
use App\Models\Product;
use App\Services\OrderService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OrderController extends Controller
{

    //
    /**
     * @var OrderService
     */
    private $service;

    public function __construct()
    {
        $this->service = new OrderService();
    }

    public function index(Request $request)
    {

    }

    public function store(OrderRequest $request)
    {
        $data = $request->validated();

        $data['order_status_id'] = 1;

        $data['end_time'] = Carbon::parse($data['start_time'])->addHours(Product::find($data['product_id'])->duration);

        if ($request->user()) $data['user_id'] = $request->user()->id;


        return $this->service->registerMasters(Order::create($data),$data['masters']);



    }
}
