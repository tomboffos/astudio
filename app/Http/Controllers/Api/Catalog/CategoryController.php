<?php

namespace App\Http\Controllers\Api\Catalog;

use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\ProductResource;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    //
    public function index()
    {
        return CategoryResource::collection(Category::orderBy('order','asc')->get());
    }

    public function show(Category $category,Request $request)
    {
        return ProductResource::collection($category->products);
    }
}
