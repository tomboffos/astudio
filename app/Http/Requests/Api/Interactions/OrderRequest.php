<?php

namespace App\Http\Requests\Api\Interactions;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'payment_type_id' => 'required|exists:payment_types,id',
            'start_time' => 'required',
            'product_id' => 'required|exists:products,id',
            'masters' => 'required'
        ];
    }
}
