<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MasterResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'orders' => $this->orders,
            'busy_times' => $this->freeTime(),
            'avatar' => str_contains($this->avatar,'https://') ? $this->avatar : asset('storage/'.$this->avatar)
        ];
    }
}
