<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'image' => str_contains($this->image,'https://') ? $this->image : asset("storage/$this->image"),
            'price' => $this->price,
            'duration' => $this->duration,
            'master_quantity' => $this->master_quantity,
            'masters' => MasterResource::collection($this->masters)
        ];
    }
}
