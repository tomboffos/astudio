<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends \TCG\Voyager\Models\User
{
    use HasFactory, Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'avatar',
        'balance',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function orders()
    {
        return $this->belongsToMany(Order::class, 'master_orders');
    }

    public function freeTime()
    {
        $data = [];
        $orders = $this->belongsToMany(Order::class, 'master_orders')->get();
        $hours = Carbon::now();


        foreach ($orders as $order){
            $hours = Carbon::parse($order['end_time'])->diffInHours($order['start_time']);
            for ($i = 0; $i<=$hours; $i++){
                $data[] = Carbon::parse($order['start_time'])->addHours($i)->toDateTimeLocalString();
            }
        }
        return $data;

    }
}
