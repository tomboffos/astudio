<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable = [
        'name',
        'email',
        'phone',
        'order_status_id',
        'payment_type_id',
        'start_time',
        'end_time',
        'user_id',
        'product_id'
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function masters()
    {
        return $this->belongsToMany(User::class,'master_orders');
    }

    public function status()
    {
        return $this->belongsTo(OrderStatus::class);
    }

    public function payment_type()
    {
        return $this->belongsTo(PaymentType::class);
    }
}
