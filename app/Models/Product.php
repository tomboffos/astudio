<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable = [
        'name',
        'description',
        'images',
        'image',
        'price',
        'order',
        'duration',
        'master_quantity'
    ];


    public function masters()
    {
        return $this->belongsToMany(User::class,'users_products');
    }

}

