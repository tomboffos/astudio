<?php

namespace Tests\Feature;

use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class OrderRequestTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->postJson('/api/orders', [
            'name' => 'Асыл',
            'phone' => '+7 707 303 99 17',
            'email' => 'tomboffos@gmail.com',
            'payment_type_id' => '1',
            'start_time' => Carbon::now(),
            'product_id' => Product::inRandomOrder()->first()->id,
            'masters' => [
                1,
            ]
        ]);

        $response->dump();

        $response->assertStatus(201);
    }
}
