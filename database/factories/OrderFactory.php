<?php

namespace Database\Factories;

use App\Models\Order;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Order::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'phone' => $this->faker->phoneNumber,
            'order_status_id' => 2,
            'start_time' => Carbon::now(),
            'end_time' => Carbon::now()->addHours(2),
            'product_id' => Product::inRandomOrder()->first()->id,
            'payment_type_id' => 1
        ];
    }
}
