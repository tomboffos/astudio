<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'name' => $this->faker->word,
            'description' => $this->faker->text,
            'images' => $this->faker->text,
            'image' => $this->faker->imageUrl(),
            'price'  => mt_rand(400,5000),
            'duration' => mt_rand(1,5),
            'master_quantity' => mt_rand(1,2)
        ];
    }
}
