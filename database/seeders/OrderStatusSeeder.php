<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrderStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('order_statuses')->insert([
            'id' => 1,
            'name' => 'Новый заказ'
        ]);

        DB::table('order_statuses')->insert([
            'id' => 2,
            'name' => 'Остался час'
        ]);

        DB::table('order_statuses')->insert([
            'id' => 3,
            'name' => 'Осталось 15 минут'
        ]);

        DB::table('order_statuses')->insert([
            'id' => 4,
            'name' => 'Заказ выполняется'
        ]);

        DB::table('order_statuses')->insert([
            'id' => 5,
            'name' => 'Заказ выполнен'
        ]);
    }
}
