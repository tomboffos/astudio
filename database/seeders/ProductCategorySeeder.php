<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        for($i=0;$i<=20;$i++)
            DB::table('products_categories')->insert([
                'product_id' => Product::inRandomOrder()->first()->id,
                'category_id' => Category::inRandomOrder()->first()->id
            ]);
    }
}
